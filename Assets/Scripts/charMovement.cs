﻿using UnityEngine;
using System.Collections;

public class charMovement : MonoBehaviour
{
    public Rigidbody rigid;

    public bool m_isClimbing;

    // Use this for initialization
    void Start ()
	{
        rigid = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector2 m_Input = new Vector2(horizontal, vertical);

        if (m_Input.sqrMagnitude > 1)
        {
            m_Input.Normalize();
        }

	    Vector3 desiredMove;

        if (m_isClimbing)
	    {
             desiredMove = transform.up * m_Input.y + transform.right * m_Input.x;
        }
	    else
	    {
            desiredMove = transform.forward * m_Input.y + transform.right * m_Input.x;
        }

	    transform.position += desiredMove*Time.deltaTime*4;


	    if (m_isClimbing)
	    {
	        RaycastHit[] hits;
            Debug.DrawRay(this.transform.position, -transform.up*2,Color.blue);
            hits = Physics.RaycastAll(this.transform.position, -transform.up, 2f);

	        for (int i = 0; i < hits.Length; i++)
	        {
	            if (hits[i].transform.tag.Equals("wall"))
	            {
                    m_isClimbing = false;
                    Debug.Log("on top");
	                return;
	            }
	        }


	    }
	}
}
