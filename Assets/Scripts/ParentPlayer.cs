﻿using UnityEngine;
using System.Collections;

public class ParentPlayer : MonoBehaviour {



	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Walkable")
        {
            transform.SetParent(other.transform);
        }
    }

    void OnTriggerExit(Collider other)
    {
        transform.SetParent(null);
    }
}
