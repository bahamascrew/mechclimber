﻿using UnityEngine;
using System.Collections;

public class climb : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag.Equals("Player"))//&& m_Camera.transform.localEulerAngles.x > 250 && m_Camera.transform.localEulerAngles.x < 300)
        {
            charMovement character = other.gameObject.GetComponent<charMovement>();

            character.m_isClimbing = true;
            other.transform.parent = transform;
            character.rigid.useGravity = false;
            Debug.Log("climbing");

        }
    }

    void OnTriggerExit(Collider collisionInfo)
    {
        if (collisionInfo.transform.tag.Equals("Player"))//&& m_Camera.transform.localEulerAngles.x > 250 && m_Camera.transform.localEulerAngles.x < 300)
        {
            charMovement character = collisionInfo.gameObject.GetComponent<charMovement>();

            character.m_isClimbing = false;
            collisionInfo.transform.parent = null;
            character.rigid.useGravity = true;
            Debug.Log("not climbing");
        }
    }
    

}
