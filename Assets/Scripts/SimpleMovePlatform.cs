﻿using UnityEngine;
using System.Collections;

public class SimpleMovePlatform : MonoBehaviour{

    public Vector3 StartPosition;
    public Vector3 EndPosition;
    public float FreezeTime = 3f;

    private float currentTime;
    private bool GoingUp;
    private bool Paused;
    private float LerpTime;
    private float PauseTimer;

    // Use this for initialization
    void Start () {
        currentTime = 0;
        GoingUp = true;
        LerpTime = Vector3.Distance(StartPosition, EndPosition) / 2;
        PauseTimer = 0;
        Paused = true;
        transform.position = StartPosition;
	}
	
	// Update is called once per frame
	void Update () {

        if (currentTime > 1.01f)
        {
            Paused = true;
            GoingUp = false;
            currentTime = 1f;
        }
        else if (currentTime < -0.01f)
        {
            GoingUp = true;
            Paused = true;
            currentTime = 0f;
        }

        if (Paused)
        {
            PauseTimer += Time.deltaTime;

            if (PauseTimer > FreezeTime)
            {
                Paused = false;
                PauseTimer = 0;
            }
        }
        else
        {
            if (GoingUp)
            {
                currentTime += Time.deltaTime / LerpTime;
            }
            else
            {
                currentTime -= Time.deltaTime / LerpTime;
            }
            transform.position = Vector3.Lerp(StartPosition, EndPosition, currentTime);
        }
	}
}
