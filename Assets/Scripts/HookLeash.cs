﻿using System;
using UnityEngine;
using System.Collections;

public class HookLeash : MonoBehaviour {

    private LineRenderer lineRenderer;
    private Color c1 = Color.yellow;
    private Color c2 = Color.red;
    private int vertexOnLine = 2;
    [SerializeField] private float width = 0.2f;

    // Use this for initialization
    void Start () {
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        lineRenderer.SetWidth(width, width);
        lineRenderer.SetVertexCount(vertexOnLine);
    }

    public void SetPosition(int id, Vector3 pos)
    {
        lineRenderer.SetPosition(id, pos);
    }

    public void SetStartPosition(Vector3 pos)
    {
        lineRenderer.SetPosition(0, pos);
    }

    public void SetEndPosition( Vector3 pos)
    {
        lineRenderer.SetPosition(vertexOnLine-1, pos);
    }

    public void SetEnabled(bool stat)
    {
        lineRenderer.enabled = stat;
    }

    // Update is called once per frame
    void Update () {
	
	}
}
