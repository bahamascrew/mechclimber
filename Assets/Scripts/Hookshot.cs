﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using System;

public class Hookshot : MonoBehaviour
{
    private float distance = 20;
    private Transform Character;
    private Rigidbody rCharacter;
    private FirstPersonController fpsController;
    private RigidbodyFirstPersonController rigFPSController;

    private bool hookReturning = false;
    private bool isHookConnected;
    private bool inHookMovement;
    private Transform connectedTarget;
    private Vector3 hookedPosition;
    private HookLeashVfx leash;
    private Transform hookHead;
    private Vector3 hookHeadOriginalPos;
    private Quaternion hookHeadOriginalRotation;
    private Vector3 hookHeadOriginalScale;

    [SerializeField] private float pullForce = 1000f;
    [SerializeField] private float CharacterSpeed = 5f;
    [SerializeField] private float HookSpeed = 50f;
    [SerializeField] private float HookReturnSpeed = 15f;

    private const string PULLABLE_OBJECT_TAG = "weakPoint";
    private const string CLIMB_POINT_TAG = "climbPoint";
    private const string GRAPPLE_POINT_TAG = "grabPoint";



    void Start ()
	{
	    Character = transform.parent.parent;
	    rCharacter = Character.GetComponent<Rigidbody>();
        fpsController = Character.GetComponent<FirstPersonController>();
	    rigFPSController = Character.GetComponent<RigidbodyFirstPersonController>();
        isHookConnected = false;
        inHookMovement = false;
        leash = GetComponentInChildren<HookLeashVfx>();
        hookHead = transform.FindChild("GrappleGunHead"); //GOD this is awful
        hookHeadOriginalPos = hookHead.localPosition;
        hookHeadOriginalRotation = hookHead.localRotation;
        hookHeadOriginalScale = hookHead.localScale;
        rigFPSController.OnCollisionOccoured += HandlePlayerCollision;
	}
	
	void Update () {
        if (rigFPSController.Grabbing && rigFPSController.Jumping)
        {
            rCharacter.useGravity = true;
            rigFPSController.Grabbing = false;
        }

        if (isHookConnected && connectedTarget!=null)
	    {
            leash.SetEnabled(true);
            //leash.SetStartPosition(this.transform.position); using new system
            if (Input.GetMouseButtonDown(0) && !inHookMovement)
	        {
                if (connectedTarget.tag.Equals(CLIMB_POINT_TAG))
                {
                    StartCoroutine(Move(hookHead));
                }else if (connectedTarget.tag.Equals(GRAPPLE_POINT_TAG))
                {
                    StartCoroutine(MoveAndGrab(hookHead));
                }
                else if (connectedTarget.tag.Equals(PULLABLE_OBJECT_TAG))
                {
                    PullObject(connectedTarget);
                }
                
            }
	        if (Input.GetMouseButtonDown(1))
	        {
                if(!inHookMovement && connectedTarget.tag.Equals(PULLABLE_OBJECT_TAG))
                {
                    DisconnectGrapple();
                }
                else
                {
                    StartCoroutine(ReturnHookToArm());
                }
            }

            if (Vector3.Distance(hookHead.position, transform.position) > distance)
            {
                StartCoroutine(ReturnHookToArm());
            }
            else if(!inHookMovement)
            {
                RaycastHit hit;
                Ray ray = new Ray(transform.position, (hookHead.position - transform.position));

                if (Physics.Raycast(ray, out hit, distance, LayerMask.NameToLayer("Player")))
                {
                    if (Vector3.Distance(hookHead.position, hit.point) > 1f)
                    {
                        StartCoroutine(ReturnHookToArm());
                    }
                }
            }
        }
        else
	        if (Input.GetMouseButtonDown(0) && !isHookConnected)
	        {
	            RaycastHit hit;

	            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

	            if (Physics.Raycast(ray, out hit, distance))
	            {
	                bool hitInteractablePoint = hit.transform.tag.Equals(CLIMB_POINT_TAG) ||
	                                            hit.transform.tag.Equals(GRAPPLE_POINT_TAG) ||
	                                            hit.transform.tag.Equals(PULLABLE_OBJECT_TAG);

                    if (hitInteractablePoint)
	                {
                        //Move the Hook Head/Claw to the interactablePoint location
	                    StartCoroutine(MoveHookHead(hit.transform,hit.point));
	                }
	            }
	        }
        
	}

    void GetReadyToMoveTowardsHook()
    {
        inHookMovement = true;
        rigFPSController.HookedAndMoving = inHookMovement;
        rCharacter.useGravity = false;
        rCharacter.velocity = Vector3.zero;
    }

    void DisconnectGrapple()
    {
        StopAllCoroutines();
        hookReturning = false;
        isHookConnected = false;
        inHookMovement = false;
        rigFPSController.HookedAndMoving = inHookMovement;
        hookHead.SetParent(this.transform);
        hookHead.localPosition = hookHeadOriginalPos;
        hookHead.localRotation = hookHeadOriginalRotation;
        hookHead.localScale = hookHeadOriginalScale;
        connectedTarget = null;
        leash.SetEnabled(false);
    }

    IEnumerator ReturnHookToArm()
    {
        if (!hookReturning)
        {
            hookReturning = true;
            while (Vector3.Distance(hookHead.position, transform.position) > 0.25f)
            {
                hookHead.SetParent(null);
                hookHead.position = Vector3.Lerp(hookHead.position, transform.position, Time.deltaTime * HookReturnSpeed);

                yield return new WaitForEndOfFrame();
            }
            DisconnectGrapple();
        }
    }

    IEnumerator MoveHookHead(Transform objectToParentTo,Vector3 hookPosition)
    {
        //Set the correct offset from the transforms position compared to the hookPosition
        Vector3 offset = hookPosition - objectToParentTo.position;

        leash.SetEnabled(true); 
        while (Vector3.Distance(hookHead.position, objectToParentTo.position + offset) > 0.5f)
        {
            if (hookReturning)
                break;
            hookHead.position += (objectToParentTo.position + offset - hookHead.position).normalized * HookSpeed *
                                     Time.deltaTime;
            yield return null;
        }
        connectedTarget = objectToParentTo;
        hookHead.parent = objectToParentTo; //lock hook head to target
        isHookConnected = true;
    }

    IEnumerator Move(Transform hook)
    {
        GetReadyToMoveTowardsHook();
        while (Vector3.Distance(rCharacter.position, hook.position) > 0.75f)
        {
            if (hookReturning)
                break;
            rCharacter.MovePosition(Vector3.Lerp(rCharacter.position, hook.position, Time.deltaTime * CharacterSpeed));
            yield return new WaitForFixedUpdate();
        }
        rCharacter.useGravity = true;
        DisconnectGrapple();
    }

    IEnumerator MoveAndGrab(Transform hook)
    {
        GetReadyToMoveTowardsHook();
        rigFPSController.Grabbing = true;
        while (Vector3.Distance(rCharacter.position, hook.position) > 0.75f)
        {
            if (hookReturning)
            {
                rigFPSController.Grabbing = false;
                break;
            }
            rCharacter.MovePosition(Vector3.Lerp(rCharacter.position, hook.position, Time.deltaTime * CharacterSpeed));
            yield return new WaitForFixedUpdate();
        }
        DisconnectGrapple();
    }

    void PullObject(Transform weakPoint)
    {
        Rigidbody rweakPoint = weakPoint.GetComponent<Rigidbody>();
        rweakPoint.isKinematic = false;
        rweakPoint.AddForce((transform.position - weakPoint.position).normalized * rweakPoint.mass * pullForce);
        rweakPoint.useGravity = true;
        weakPoint.parent = null;
        StartCoroutine(ReturnHookToArm());
    }

    void HandlePlayerCollision(object collision, EventArgs e)
    {
        if (((Collision)collision).transform != hookHead.parent)
        {
            StopAllCoroutines();
            rCharacter.useGravity = true;
            inHookMovement = false;
            rigFPSController.HookedAndMoving = inHookMovement;
            DisconnectGrapple();
        }
    }

    
}
