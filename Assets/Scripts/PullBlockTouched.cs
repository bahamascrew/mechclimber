﻿using UnityEngine;
using System.Collections;

public class PullBlockTouched : MonoBehaviour {

    private Rigidbody rbody;
	// Use this for initialization
	void Start () {
        rbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter(Collision other)
    {
        if(other.collider.CompareTag("weakPoint"))
        {
            rbody.useGravity = true;
            transform.parent = null;
        }
    }
}
