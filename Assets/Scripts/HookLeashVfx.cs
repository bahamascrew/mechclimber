﻿using UnityEngine;
using System.Collections;

//Scnoobi is sexy
public class HookLeashVfx : MonoBehaviour
{

    public Transform target;
    public int zigs = 100;
    public float speed = 1f;
    public float scale = 1f;
    public Light startLight;
    public Light endLight;

    [SerializeField] private Color particleTint = Color.white;
    private Color disabledColor = new Color(0, 0, 0, 0);

    private Perlin noise;
    private float oneOverZigs;

    public ParticleSystem.Particle[] particles;

    private ParticleSystem pSystem;

    private bool activeParticles = false;

	void Start ()
	{
        InitializeIfNeeded();
        
	    oneOverZigs = 1f/(float) zigs;
        pSystem.Stop();

        //Emite the current amount of zigs
        pSystem.Emit(zigs);
	    particles = new ParticleSystem.Particle[pSystem.maxParticles];
	    
	}
	
	void Update () 
    {
        InitializeIfNeeded();

	    if (noise == null)
	        noise = new Perlin();

        //Seems to just be an escalation of force.
        float timex = Time.time * speed * 0.1365143f;
        float timey = Time.time * speed * 1.21688f;
        float timez = Time.time * speed * 2.5564f;

	    var aliveParticles = pSystem.GetParticles(particles);

	    for (int i = 0; i < aliveParticles; i++)
	    {
            Vector3 position = Vector3.Lerp(transform.position, target.position, oneOverZigs * (float)i);

            //Randomize positions
            Vector3 offset = new Vector3(noise.Noise(timex + position.x, timex + position.y, timex + position.z),
                                        noise.Noise(timey + position.x, timey + position.y, timey + position.z),
                                        noise.Noise(timez + position.x, timez + position.y, timez + position.z));
            //Keeps the earlier particles closer to the starting position and super wild towards the end
            position += (offset * scale * ((float)i * oneOverZigs));

            particles[i].position = position;
            
	        particles[i].lifetime = 1f;
            if (activeParticles)
                particles[i].startColor = particleTint;
            else
                particles[i].startColor = disabledColor;
	    }

        //updates the particle system
        pSystem.SetParticles(particles,aliveParticles);

        
	    if (pSystem.particleCount >= 2)
	    {
	        if (startLight)
	            startLight.transform.position = particles[0].position;
	        if (endLight)
	            endLight.transform.position = particles[particles.Length - 1].position;
	    }
	}

    void InitializeIfNeeded()
    {
        if (pSystem == null)
            pSystem = GetComponent<ParticleSystem>();
        if(particles == null || particles.Length < pSystem.maxParticles)
            particles = new ParticleSystem.Particle[pSystem.maxParticles];
    }

    public void SetEnabled(bool state)
    {
        activeParticles = state;
    }
}
